import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CursosService {

  constructor() { }

  getCursos(){
    return [{id: 1, nome: 'Bruno'},{id: 2, nome: 'Mario'}];
  }
}
